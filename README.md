# org.openjaus.configeditor

The OpenJAUS Configuration Editor helps users create the configuration files (.ojconf) needed for use with the OpenJAUS SDK. 

* Download the tool on the [Downloads](https://bitbucket.org/openjaus/org.openjaus.configeditor/downloads/) page.
* Documentation on using the tool can be found at [support.openjaus.com](http://support.openjaus.com)